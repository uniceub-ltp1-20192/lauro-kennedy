from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.entidadeFilha import Cadastrante

class Menu:
    @staticmethod
    def menuPrincipal():
        print("""
            Menu do hospital:

            0 - Sair
            1 - Consultar
            2 - Inserir
            3 - Alterar
            4 - Excluir 
            """)
        return Validador.validar("[0-4]",
        """Menu não encontrado por favor informe uma das opções entre: {}""",
        """Opcao {} Valida. \n """)
        

    @staticmethod
    def menuConsultar():
        return input("""
                0 - Voltar
                1 - Consultar por Identificador
                2 - Consultar por Atributo
                   
informe sua opção:  """)


    @staticmethod
    def iniciarMenu():
        opMenu = ""
        d = Dados()
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            print(retorno)
                       
                        else:
                            print("Nao encontrado.")
                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        if retorno != None:
                            print(retorno)
                       # else:
                           # print("Nao encontrado.")
                    elif opMenu == "0":
                        print("voltando...")
                    else:
                        print("Digite uma opcao valida!")  
                opMenu = ""            
            elif opMenu == "2":
                print("Janela para Inserir:\n")
                Menu.menuInserir(d)
                opMenu = ""
            elif opMenu == "3":
                print("Janela para Alterar:\n")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("Nao encontrado.")

                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("Nao encontrado.")
                    elif opMenu == "0":
                        print("voltando...")
                    else:
                        print("Digite uma opcao valida!")
                opMenu = ""
            elif opMenu == "4":
                print("Janela para Excluir.")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        Menu.menuDeletar(d,retorno)
                        
                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        Menu.menuDeletar(d,retorno)
                        
                    elif opMenu == "0":
                        print("voltando...")
                    else:
                        print("Digite uma opcao valida!") 
                opMenu = ""
            elif opMenu == "0":
                print("Fechando menu.")
            else:
                print("Digite uma opcao valida!")

    @staticmethod
    def menuInserir(d):
        convenios = dict()
        convenios = {'cassi':100,'amil':100,'bradesco saude': 100,'notredame intermedio': 75,
        'porto seguro':75,'sulamerica saude':75,'care plus':50,'prevent senior':50,
        'allianz saude':50,'hapvida':25,'sompo saude':25,'omint': 25,}
      
      
        entidadeFilha = Cadastrante()
        entidadeFilha.nome = input("Informe o nome: ")
        entidadeFilha.sexo = input("Informe o sexo: ")
        entidadeFilha.idade = input("Informe a idade: ")
        entidadeFilha.cpf = input("Informe o CPF: ")
        entidadeFilha.rg = input("Informe o RG: ")
        entidadeFilha.planoSaude = input("Qual o seu plano de saude?: ")

        def checkKey(convenios, key):
            if key in convenios.keys():
                print('Seu convenio tem um desconto de',convenios[key],'%')
            else:
                print('Seu convenio nao tem desconto.')
        
        key = entidadeFilha.planoSaude
        checkKey(convenios, key)


        input("pressione enter para confirmar.")
        

        d.inserirDado(entidadeFilha)
    


    @staticmethod
    def menuAlterar(retorno,d):
        print(retorno)
        retorno.nome = Validador.validarValorEntrada(retorno.nome,"Informe o nome: ")
        retorno.sexo =  Validador.validarValorEntrada(retorno.sexo,"Informe o sexo: ")
        retorno.idade = Validador.validarValorEntrada(retorno.idade,"Informe a idade: ")
        retorno.cpf = Validador.validarValorEntrada(retorno.cpf,"Informe o CPF: ")
        retorno.rg = Validador.validarValorEntrada(retorno.rg,"Informa o RG: ")
        retorno.planoSaude = Validador.validarValorEntrada(retorno.planoSaude,"Qual o seu plano de saude?: ")
      
        d.alterarDado(retorno)


    @staticmethod
    def menuDeletar(d,entidade):
        print(entidade)
        resposta = input(
            """Deseja excluir o cadastrante?
            S - Sim
            N - Nao

            Sua decisao: """)
        if(resposta == "S" or  resposta == "s"):
            d.deletar(entidade)


    @staticmethod
    def menuBuscaPorIdentificador(d):
        retorno = d.buscarPorIdentificador(
            Validador.validar(r'\d+','',''))
        return retorno

    @staticmethod
    def menuBuscarPorAtributo(d : Dados):
        retorno = d.buscarPorAtributo(
            input("Informe o seu plano de saude: "))
        print(retorno)
