from Entidades.entidadeFilha import Cadastrante
from Validador.validador import Validador

class Dados:

    def __init__(self):
        self._dados = dict()
        self._identificador = 0

    def geradorIdentificador(self):
        self._identificador = self._identificador + 1
        return self._identificador

    def buscarPorIdentificador(self,identificador):
        if (len(self._dados) == 0):
            print("Sem cadastrantes!")
        else:
            return self._dados.get(int(identificador))

    def buscarPorAtributo(self,param):
        lista = dict()
        ultimoEncontrado = 0
        if (len(self._dados) == 0):
            print("Sem cadastrantes!")
        else:
            for x in self._dados.values():
                if x.planoSaude == param:
                    lista[x.identificador] = x
                    ultimoEncontrado = x.identificador
            if (len(lista) == 1):
                return lista[ultimoEncontrado]
            else:
                if (len(lista) == 0):
                    print("Nenhum encontrado!")
                    return None 
                return self.buscarPorIdentificadorComLista(lista)

    def buscarPorIdentificadorComLista(self,lista):
        print("\nExistem mais de um cadastrante com o filtro informado:")
        for x in lista.values():
            print(x)
        print("Informe o numero do identificador:")
        return lista.get(int(Validador.validar("\d+","Informe um valor do tipo inteiro:","")))
    
    def inserirDado(self,entidade):
        self._ultimoIdentificador = self.geradorIdentificador()
        entidade.identificador = self._ultimoIdentificador
        print(entidade)
        self._dados[self._ultimoIdentificador] = entidade
    
    def alterarDado(self,entidade):
        self._dados[entidade.identificador] = entidade

    def deletar(self,entidade):
        print(entidade)
        del self._dados[entidade.identificador]


