import re

class Validador:
    @staticmethod
    def validar(expReg,msgInvalido, msgValido):
        teste = False
        while teste == False:
            valor = input("Informe o número : ")
            verificarEx = re.match(expReg,valor)
            if verificarEx == None:
                print(msgInvalido.format(expReg))
            else:
                print(msgValido.format(valor))
                return valor

    @staticmethod
    def validarValorEntrada(valorAtual,msg):
        novoValor = input(msg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual