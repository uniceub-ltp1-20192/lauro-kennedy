from Entidades.entidadeMae import Pessoa

class Cadastrante(Pessoa):
  def __init__(self,cpf="",rg="",planoSaude=""):
    super().__init__()
    self._cpf = cpf
    self._rg = rg
    self._planoSaude = planoSaude

  @property
  def cpf(self):
    return self._cpf

  @cpf.setter
  def cpf(self,cpf):
    self._cpf = cpf

  @property
  def rg(self):
    return self._rg

  @rg.setter
  def rg(self,rg):
    self._rg = rg

  @property
  def planoSaude(self):
    return self._planoSaude

  @planoSaude.setter
  def planoSaude(self,planoSaude):
    self._planoSaude = planoSaude
  

  def cadastrar(self):
    print("Me cadastrei")


  def __str__(self):
    return input( """\nDados do Cadastrante:
  Identificador: {}
  Nome: {}
  Sexo: {}
  Idade: {}
  CPF: {}
  RG: {}
  Plano de Saude: {}
  pressione enter para confirmar.
    """.format(self.identificador
    ,self.nome,
    self.sexo,
    self.idade,
    self.cpf,
    self.rg,
    self.planoSaude))