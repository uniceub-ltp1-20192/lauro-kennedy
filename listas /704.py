# exercicio 4

convidados = ['lauro','milena','anna julia','michelle']

def convite(n):
	print(convidados[n].title(),"vc esta convidado para meu\njantar em minha casa amanha a noite.\n ")


x1 = convite(0)
x2 = convite(1)
x3 = convite(2)
x4 = convite(3)

print("--------------------------------------------------------------------\n")

def n_comparecer(n):
	print (convidados[n].title()," nao pode ir ao jantar.\n")

z1 = n_comparecer(0)
z2 = n_comparecer(1)
z3 = n_comparecer(2)


print("--------------------------------------------------------------------\n")

convidados[0] = 'joze'
convidados[1] = 'cebola'
convidados[2] = 'laranja'

y1 = convite(0)
y2 = convite(1)
y1 = convite(2)

print("--------------------------------------------------------------------\n")

print('consegui achar uma mesa maior, agora posso chamar mais 3 pessoas.')

convidados.insert(0,'caio')
convidados.insert(3,"maria")
convidados.append('cleisson')

print("--------------------------------------------------------------------\n")

nv1 = convite(0)
nv2 = convite(1)
nv3 = convite(2)
nv4 = convite(3)
nv5 = convite(4)
nv6 = convite(5)
nv7 = convite(6)

print("--------------------------------------------------------------------\n")

print('infelizmente minha mesa nao chegara a\ntempo, por isso so poderei chamar 2\npessoas para o meu jantar.\n')

def desconvidar(n):
	d = convidados.pop(n)
	print(d,"infelizmente vc nao podera ir\nao meu jantar de amanha.\n")

print("--------------------------------------------------------------------\n")

d1 = desconvidar(6)
d2 = desconvidar(4)
d3 = desconvidar(2)
d4 = desconvidar(1)
d5 = desconvidar(0)

print("--------------------------------------------------------------------\n")

def permanecem(n):
	print(convidados[n], ' vc ainda esta convidada.\n')

c1 = permanecem(0)
c2 = permanecem(1)

del convidados[0]
del convidados[0]
print(convidados)