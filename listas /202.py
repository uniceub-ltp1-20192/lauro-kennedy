# exercicio 2

cebolas = 300
#recebe um valor 
cebolas_na_caixa = 120
#recebe um valor
espaco_caixa = 5
#recebe um valor
caixas = 60
#recebe um valor

cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#recebe duas variaveis e as subtrai
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
#recebe tres variaveis, divide e depois subtrai
caixas_necessarias = (cebolas_fora_da_caixa / espaco_caixa)
#recebe duas variaveis e as divide

print ("Existem " + str(cebolas_na_caixa) + ' cebolas encaixotadas')
#imprime uma mensagem com o valor de  uma variavel
print ("Existem " + str(cebolas_fora_da_caixa) + " cebolas sem caixa")
#imprime uma mensagem com o valor de  uma variavel
print ("Em cada caixa cabem " + str(espaco_caixa) + " cebolas")
#imprime uma mensagem com o valor de  uma variavel
print ("Ainda temos, " +str(caixas_vazias) +" caixas vazias")
#imprime uma mensagem com o valor de  uma variavel
print ("Então, precisamos de " + str(caixas_necessarias) + " caixas para empacotar todas as cebolas")
#imprime uma mensagem com o valor de  uma variavel